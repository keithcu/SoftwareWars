import os, sys, re, argparse, collections, datetime, time
import timeit, pickle

#Trying to replace this behavior on the Linux shell
#echo Arch on Lenovo
#grep -G 'p=3270 ' access.log | awk '{print $1}' | sort | uniq | wc -l

#This code is slow, but it runs in 0.3 seconds now on a 11MB file so I don't
#care much. Interestingly the CPython and PyPy performance are currently about
#the same. (0.33s versus 0.32s) On my Linode machine, the difference is larger:
#(0.55s versus 0.36s) It could be the different versions of the tools
#(Debian vs Arch)

#Update - added pickling to save the current state of the apache parse
#Now the code is still slow, but it caches the results so it never has to go through 
#the entire file except once per week. It runs at lightning speed!

def FindAddStringRe(results, key, line, s):
    lin = re.findall(s, line)
    if lin:
        index = line.find(' ')
        ip = line[0:index]

        result = results.get(key)
        result.add(ip)
        return True
    return False

def FindAddString(results, key, line, s):
    if line is None:
        results[key] = set()
        return False

    lin = line.find(s)
    if lin > 0:
        result = results.get(key)
        index = line.find(' ')
        ip = line[0:index]
        result.add(ip)
        return True
    return False

def main():
    filename = 'access.log'
    ap = argparse.ArgumentParser()
    ap.add_argument('-l', '--last', action='store_true', help='Last Stats')
    args = ap.parse_args()

    if args.last:
        filename = 'access.log.1'

    f = open(filename, 'r')

    results = collections.OrderedDict()
    try:
        filestart = 0
        fcache = open(filename + '.cache', 'rb')
        results = pickle.load(fcache)
        filestart = results['ApacheCurrentLength']
        print ("Found cache, restarting at " + str(filestart))

        # TODO: if file is old, need to not use it. 
        # cachedate = time.ctime(os.path.getctime(filename))
        # print ("Cache date: " + cachedate.strftime("%Y-%m-%d %H:%M"))

        if filestart > os.path.getsize(filename):
            print ("New week, old cache needs to be cleaned")
            raise ValueError('Need a new cache')
            
        f.seek(filestart)
    except:
        print ("Gotta do a full search")
        #Prep the OrderedDict
        #Rather than pollute FindAddString() checking for the key every time
        #I have it setup everything here. That saves .4 seconds with CPython.
        FindAddString(results, 'Home Page', None, None)
        FindAddString(results, 'Downloads', None, None)
        FindAddString(results, 'Book HTML', None, None)
        FindAddString(results, 'Space Elevator Wiki', None, None)
        FindAddString(results, 'Space Elevator Less 7', None, None)
        FindAddString(results, 'Apache OpenOffice Created', None, None)
        FindAddString(results, 'LibreOffice Brands', None, None)
        FindAddString(results, 'Arch on Yoga', None, None)
        FindAddString(results, 'Manjaro', None, None)
        FindAddString(results, 'Arch Wallpaper', None, None)
        FindAddString(results, 'LibreOffice Patches', None, None)
        FindAddString(results, 'Movie Public', None, None)
        FindAddString(results, 'PyTorch vs Copyleft', None, None)

    print (time.strftime("%Y-%m-%d %H:%M"))
    start_time = timeit.default_timer()

    for line in f:
        FindAddString(results, 'Home Page', line, 'page_id=407')
        regexp = 'SoftwareWars.pdf|SoftwareWars.epub|SoftwareWars-es.pdf|SoftwareWars-es.epub|SoftwareWars-pt.pdf|SoftwareWars-cn.pdf|SoftwareWarsExcerpt.pdf|SoftwareWarsLinux.pdf'
        FindAddStringRe(results, 'Downloads', line, regexp)
        
        #         Space Eleva  Ubuntu Creat Linux     AI & Google Apple        Java         Patents
        regexp = 'page_id=417|page_id=558|page_id=599|page_id=603|page_id=1165|page_id=2228|page_id=1548'
        FindAddStringRe(results, 'Book HTML', line, regexp)
        
        FindAddString(results, 'Space Elevator Wiki', line, r'spaceelevatorwiki')
        FindAddString(results, 'Space Elevator Less 7', line, r'SpaceElevatorConference_html_m34c83a16.jpg')
        FindAddString(results, 'Apache OpenOffice Created', line, 'p=2567')
        FindAddString(results, 'LibreOffice Brands', line, 'p=3163')
        FindAddStringRe(results, 'Arch on Yoga', line, 'p=3270|p=3641|p=3739')
        FindAddString(results, 'Manjaro', line, 'p=3389')
        FindAddString(results, 'Arch Wallpaper', line, 'ArchCycles.jpg')
        FindAddStringRe(results, 'LibreOffice Patches', line, 'p=3444|p=3580')
        FindAddString(results, 'Movie Public', line, 'MoviePublic')
        FindAddString(results, 'PyTorch vs Copyleft', line, 'TensorflowvsPyTorch.png')

    print(timeit.default_timer() - start_time)

    # Save current progress out to disk
    filesize = f.tell()
    results['ApacheCurrentLength'] = filesize
    fcache = open(filename + '.cache', 'wb')
    pickle.dump(results, fcache, 0)

    #Delete entry to not confuse drawing code below
    del(results['ApacheCurrentLength'])

    for k, v in results.items():
        print ("{:<30} {}".format(k, str(len(v))) )

if __name__ == "__main__":
    main()

