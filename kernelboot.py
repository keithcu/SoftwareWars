import time
import random
from asciimatics.screen import Screen

def printOK(screen, row):
   screen.print_at('[  ', 0, row, colour=Screen.COLOUR_WHITE)
   screen.print_at('OK', 3, row, colour=Screen.COLOUR_GREEN)
   screen.print_at('  ]', 5, row, colour=Screen.COLOUR_WHITE)

Initial = ["starting systemd version 232", 
":: running hook [udev]",
":: Trigger uevents...",
":: performing fsck on '/dev/sda1'",
"/dev/sda1: recovering journal",
]

Main = ["Listening on udev Kernel Socket",
"Reached target Remote File Systems",
"Reached target Login Prompts.",
"Listening on LVM2 metadata daemon socket.",
"Listening on Journal Socket.",
"Created slice System Slice",
"Bluetooth: HCI device and connection manager initialized",
"Started Load/Save Screen Backlight Brightness of backlight:intel_backlight",
"Mounting /home...",
"Started Create Volatile Files and Directories.",
"Starting Network Time Synchronization...",
"Listening on D-Bus System Message Bus Socket.",
"Starting Login Service...",
"Starting Network Manager...",
"Starting Save/Restore Sound Card State...",
"Loading rules from directory /etc/polkit-1/rules.d",
"Started Authorization Manager.",
"Starting User Manager for UID 120...",
"Started Accessibility services bus.",
"Started RealtimeKit Scheduling Policy Service.",
]

shortsleep = 0.100

def demo(screen):

   row = 0

   time.sleep(1)

   for s in Initial:
      screen.print_at(s, 0, row, colour=Screen.COLOUR_WHITE)
      row = row + 1
      time.sleep(random.randint(1,10) / 100)
      screen.refresh()   

   row = row + 1
   screen.print_at('Welcome to', 0, row, colour=Screen.COLOUR_WHITE)
   screen.print_at('Linux!', 11, row, colour=Screen.COLOUR_BLUE)
   screen.refresh()   

   row = row + 2

   time.sleep(0.50)

   for s in Main:
      printOK(screen, row)
      screen.print_at(s, 9, row, colour=Screen.COLOUR_WHITE)
      row = row + 1
      sl = random.randint(5,30) / 100
#      st = str(sl)
#      screen.print_at(st, 50, row, colour=Screen.COLOUR_WHITE)
      time.sleep(sl)
      screen.refresh()

   time.sleep(10)

Screen.wrapper(demo)
